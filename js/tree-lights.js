// ==============================================================================
// tree-lights.js
// ------------------------------------------------------------------------------
// Using the Teapot Demo from Udacity, we'll create a forest. The trees can be
// tweaked by using the UI (just their base-rotations, as these are random).
// It will also add some best lighting method that I can find.
// ------------------------------------------------------------------------------
// Angela Gross
// CSCI 441
// Trees
// ==============================================================================
/*global THREE, requestAnimationFrame, dat, window */

// Scene
var camera, scene, renderer;
var cameraControls;
var effectController;
var clock = new THREE.Clock();

// Sky 
var skyPath = "images/skybox/";
var skySize = 190000;
var skyBox;

// Lights
var bluelight, yellowlight, sunlight;

// Light age info
var blueMaxPos = 80000;
var blueMaxInt = 0.7;
var yellowMaxPos = -80000;
var yellowMaxInt = 0.8;
var sunMaxInt = 0.9;
var reset = false;

// Plane
var planeImg = "images/skybox/bottom.png";
var planeSize = skySize;

// Forest information
var numTrees = 1; // number of trees generated
var numTreeTypes = 2; // number of types of random trees (save computations)
var treeCluster = 30; // how close trees are  (the larger this is, the farther they are)
var forest;
var RotateZ, RotateX;
var forestMade = false;

// Tree information
var treeBaseRadius = 100; // cylinder base of tree (may differ due to randomness)
var treeBaseLength = 4000; // cylinder base length (may differ due to randomness)
var phi = 1.618;

// Tree data structure info
var branchFactor = 5; // max branches each trunk
var treeDepth = 5; // number of levels
var branches = [[]]; // hierarchy of branches
var trees = []; // types of trees (not all trees generated)

// Tree texture
var treeImg = "images/small-tree.jpg";
var treeTexture = THREE.ImageUtils.loadTexture(treeImg);
var treeMaterial = new THREE.MeshPhongMaterial({ map: treeTexture });

// Base leaf information
var leafSize = 20;
var leafImg = "images/leaf.jpg";
var leafTexture = THREE.ImageUtils.loadTexture(leafImg);
var leafMaterial = new THREE.MeshPhongMaterial({ map: leafTexture, transparent: true, opacity: 0.3 });
var baseLeaf = new THREE.Mesh(new THREE.CubeGeometry(leafSize, leafSize, 1), leafMaterial);

// Rotate canvas so we can see the tree better
var canvasRotate = 90 * Math.PI/180;
var yTranslate = -300;

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// MAKING TREES, FOREST, AND GROUND

// ==========================================================================================================
// makeForest()
// ----------------------------------------------------------------------------------------------------------
//  Creates a forest of randomly scattered trees (variables to fine tune forest are above)
// ==========================================================================================================
function makeForest()
{
	forest = new THREE.Object3D();

	// Make a centered tree
	if (numTrees == 1)
	{
		var tree = makeTree();
		forest.add(tree);
	}
	// Make forest of randomly scattered trees
	else
	{
		// Make types of trees
		for(var i = 0; i < numTreeTypes; i++)
		{
			trees[i]  = makeTree();		
		}
		
		// Position/clone trees
		for(var j = 0; j < numTrees; j++)
		{
			// Random tree
			var tree = trees[Math.floor(Math.random()*(numTreeTypes - 1))].clone();
				
			var randXPosition = Math.floor(Math.random()*500 - 500);
			var randZPosition  = Math.floor(Math.random()*500 - 500);
	
			tree.position.x = (randXPosition * j) + treeCluster + planeSize/5;
			tree.position.z = (randZPosition * j) + treeCluster + planeSize/5;
	
			forest.add(tree);
		}
	}
	
	// Update lighting for forest
	enableObjectShadows(forest);

	scene.add(forest);
}

// ==========================================================================================================
// makeTree()
// ----------------------------------------------------------------------------------------------------------
// Creates a tree with leaves.
// @returns THREE.Object3D tree
// ==========================================================================================================
function makeTree()
{
	// Make object
	var tree = new THREE.Object3D();

	// Make all the skinny poles for tree
	makePoles(treeBaseRadius, treeBaseLength, tree);

	// Rotate all the poles so the tree "branches out"
	rotateBranches(tree);

	// Move tree down
	tree.position.y = yTranslate;

	return tree;
}

// ==========================================================================================================
// makePoles()
// ----------------------------------------------------------------------------------------------------------
// Makes trunks (single skinny poles) for tree.
// @params int trunkWidth, int trunkHeight, THREE.Object3D tree
// ==========================================================================================================
function makePoles(trunkWidth, trunkHeight, tree)
{
	// Get random width and height modifiers
	var randWidth = Math.random()*0.55;
	var randHeight = Math.random()*0.5;
	
	// Make base trunk
	trunkHeight = trunkHeight / (3 ^ (phi - randHeight) );
	var baseTrunk = new THREE.Object3D();
	baseTrunk.height = trunkHeight;

	// Compute cylinder and move up
	var lowerRadius = trunkWidth;
	var topRadius = lowerRadius / (2 ^ (phi - randWidth) );
	var cylinderGeo = new THREE.CylinderGeometry(topRadius, lowerRadius, trunkHeight, 32);
	var cylinder = new THREE.Mesh(cylinderGeo, treeMaterial);
	cylinder.position.y = (trunkHeight / 2);

	// Update hierarchy
	baseTrunk.add(cylinder);
	tree.add(baseTrunk);

	// Start off with trunk
	branches[0][0] = baseTrunk;
	
	// Build tree by stacking up the branches (will look like asparagus)
	for(var i = 1; i <= treeDepth; i++)
	{
		// Initialize to empty list
		branches[i] = [];
		
		// Keep track of the offset of branches
		var k = 0;
		
		// Get random width and height modifiers
		randWidth = Math.random()*0.65;
		randHeight = Math.random()*1.5;
		
		// Modify for next iteration
		trunkWidth = trunkWidth / (2 ^ (phi - randWidth) );
		trunkHeight = trunkHeight / (2 ^ (phi - randHeight) );
		
		// Iterate through parent's children
		for(j = 0; j < branches[i-1].length; j++)
		{
			// Random number of branches
			var numBranches = Math.floor(Math.random()*branchFactor + 2);
			
			// Keep track of branches
			var kn = k;

			// Add branches/leaves
			while(k < numBranches + kn)
			{
				// Make branch
				if (i < treeDepth)
				{		
					// Compute cylinder (branch) and move up
					lowerRadius = trunkWidth;
					topRadius = lowerRadius / (2 ^ (phi - 1) );
					cylinderGeo = new THREE.CylinderGeometry(topRadius, lowerRadius, trunkHeight, 32);
					cylinder = new THREE.Mesh(cylinderGeo, treeMaterial);
					cylinder.position.y = trunkHeight / 2;
				
					// Maintain hierarchy and move up
					branches[i][k] = new THREE.Object3D();
					branches[i][k].add(cylinder);
					branches[i][k].height = trunkHeight;
					branches[i][k].position.y = branches[i-1][j].height;
					branches[i-1][j].add(branches[i][k]);
				}
				// Make leaf
				else
				{
					// Compute triangle (leaf)
					var leaf = baseLeaf.clone();
					
					// Maintain hierarchy and move up
					branches[i][k] = new THREE.Object3D();
					branches[i][k].add(leaf);
					branches[i][k].height = leafSize;
					branches[i][k].position.y = branches[i-1][j].height;
					branches[i-1][j].add(branches[i][k]);
				}
				
				k++;
				
			} // branch iteration

		} // children iteration

	} // depth iteration

}

// ==========================================================================================================
// rotateBranches()
// ----------------------------------------------------------------------------------------------------------
// Rotates branches so they are branching out away from tree
// @params THREE.Object3D tree
// ==========================================================================================================
function rotateBranches(tree)
{
	// Iterate through the depths
	for(var i = 1; i <= treeDepth; i++)
	{
		// Iterate through the branches
		for(var j = 0; j < branches[i].length; j++)
		{
			// Determine how branch is rotated
			branchNum = j % 4;
			randRotate = Math.floor(Math.random() * 25 - 25);
			
			// Rotate depending on placement
			if(branchNum == 1)
			{
				branches[i][j].rotation.x = (RotateX + randRotate) * Math.PI/180;
			}
			else if(branchNum == 2)
			{
				branches[i][j].rotation.x = (-RotateX + randRotate) * Math.PI/180;
			}
			else if(branchNum == 3)
			{
				branches[i][j].rotation.z = (RotateZ + randRotate) * Math.PI/180;
			}
			else if(branchNum == 0)
			{
				branches[i][j].rotation.z = (-RotateZ + randRotate) * Math.PI/180;
			}

		} // branch iteration

	} // depth iteration
}

// ==========================================================================================================
// makePlane()
// ----------------------------------------------------------------------------------------------------------
// Using a cube geometry, we'll map a texture to the plane (floor) and repeat the texture. Also rotates the
// plane along with translating it down before adding it to the scene
// ==========================================================================================================
function makePlane()
{
	var floorGeometry = new THREE.CubeGeometry(planeSize, planeSize, 10);
	var floorTexture;
	var floorMaterial;
	var floor;

	floorTexture = THREE.ImageUtils.loadTexture( planeImg );
	floorMaterial = new THREE.MeshPhongMaterial({ map: floorTexture });
	floor = new THREE.Mesh(floorGeometry, floorMaterial);

	// Move canvas
	floor.rotation.x = -canvasRotate;
	floor.position.y = yTranslate - 10/2;
	
	// Shadow
	floor.receiveShadow = true;

	scene.add(floor);
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// LIGHTING AND SKY

// ==========================================================================================================
// enableObjectShadows()
// ----------------------------------------------------------------------------------------------------------
// Recursively enables shadows for an object 3D
// @params THREE.Object3D object3d
// ==========================================================================================================
function enableObjectShadows(object3d)
{
	var numChildren = object3d.children.length;
	
	for(var i = 0; i < numChildren; i++)
	{
		var child = object3d.children[i];
		
		child.castShadow = true;
		child.receiveShadow = true;
		
		// Recurse through children of children (first child is mesh)
		if (child.children.length > 1)
		{
			enableObjectShadows(child);
		}	
	}
}

// ==========================================================================================================
// setupLights()
// ----------------------------------------------------------------------------------------------------------
// Creates light (sun and sky lights) for the forest scene.
// ==========================================================================================================
function setupLights()
{
	// SKYLIGHT
	
	// blue
	bluelight = new THREE.PointLight( 0x95D5FF, 0.1, 250000 );
	bluelight.position.set( 30000, 50000, -80000 );
	scene.add(bluelight);
	// yellow
	yellowlight = new THREE.PointLight(0xFFFF00, 0.1, 250000 );
	yellowlight.position.set( 20000, 50000, 80000 );
	scene.add(yellowlight);
	
	// SUNLIGHT
	
	// Light info
	sunlight = new THREE.DirectionalLight( 0xFFFFFF, 0.1 );
	sunlight.castShadow = true;
	sunlight.position.set(20000, 50000, 0);
	
	// Shadow info
	sunlight.shadowCameraVisible = true; // enable for debugging
	sunlight.shadowDarkness = 0.8;
	sunlight.target.position.set(0, 0, 0);
	sunlight.shadowCameraNear = 0;
	sunlight.shadowCameraFar = 100000;
	sunlight.shadowCameraLeft = -20000;
	sunlight.shadowCameraRight = 20000;
	sunlight.shadowCameraTop = 20000;
	sunlight.shadowCameraBottom = -20000;
	scene.add(sunlight);
	
	// LIGHT THAT DOESN'T WORK WELL
	//hemiLight = new THREE.HemisphereLight( 0x0000ff, 0x00ff00, 0.2 );
	//scene.add(hemiLight);
}

// ==========================================================================================================
// age()
// ----------------------------------------------------------------------------------------------------------
// Age lights from morning to high noon. The moving point lights imitates wind movement.
// ==========================================================================================================
function age()
{
	// Age bluelight
	if (bluelight.position.z < blueMaxPos)
	{
		bluelight.position.z += 50;
		
		if (bluelight.intensity < blueMaxInt)
		{
			bluelight.intensity += 0.0015;
		}
	}
	else
	{
		reset = true;
	}
	
	// Age yellowlight
	if (yellowlight.position.z > yellowMaxPos)
	{
		yellowlight.position.z -= 50;
		
		if (yellowlight.intensity < yellowMaxInt)
		{
			yellowlight.intensity += 0.002;
		}
	}
	else
	{
		reset = true;
	}
	
	// Age sunlight
	if (sunlight.intensity < sunMaxInt)
	{
		sunlight.intensity += 0.0005;
	}
	
	// Start over
	if (reset)
	{
		bluelight.position.z = -80000;
		bluelight.intensity = 0.1;
		yellowlight.position.z = 80000;
		yellowlight.intensity = 0.1;
		sunlight.intensity = 0.1;
		reset = false;
	}
		
}

// ==========================================================================================================
// addSkyBox()
// ----------------------------------------------------------------------------------------------------------
// Creates a skybox for our forest scene to live in.
// ==========================================================================================================
function addSkyBox()
{
	// Skybox images
	var sides =
	[
		skyPath + "left.png", skyPath + "right.png",
		skyPath + "top.png", skyPath + "bottom.png",
		skyPath + "front.png", skyPath + "back.png",
	];

    // Load images
    var skyTexture = THREE.ImageUtils.loadTextureCube(sides);
    skyTexture.format = THREE.RGBFormat;

    // Prepare skybox material (shader)
    var skyShader = THREE.ShaderLib["cube"];
    skyShader.uniforms["tCube"].value = skyTexture;
    var skyMaterial = new THREE.ShaderMaterial
	({
		fragmentShader: skyShader.fragmentShader,
		vertexShader: skyShader.vertexShader,
		uniforms: skyShader.uniforms,
		depthWrite: false,
		side: THREE.BackSide
    });

    // Create mesh with cube geometry and add to the scene
    skyBox = new THREE.Mesh(new THREE.CubeGeometry(skySize, skySize, skySize), skyMaterial);
	//skyBox.position.y ;
    skyBox.needsUpdate = true;
	skyBox.receiveShadow = true;

    scene.add(skyBox);
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// INITIALIZATION OF SCENE

function init() 
{	
	// CANVAS
	var canvasWidth = window.innerWidth;
	var canvasHeight = window.innerHeight;

	// CAMERA
	camera = new THREE.PerspectiveCamera( 100, window.innerWidth / window.innerHeight, 1, 150000 );
	camera.position.set( -600, 4000, 1300 );
	backgroundCamera = new THREE.Camera();

	// RENDERER
	renderer = new THREE.WebGLRenderer();
	renderer.setSize( canvasWidth, canvasHeight );
	renderer.setClearColorHex( 0xEFEFEF, 1.0 );
	
	// SHADOWS
	renderer.shadowMapEnabled = true;
	renderer.shadowMapType = THREE.PCFSoftShadowMap;
	
	var container = document.getElementById('container');
	container.appendChild( renderer.domElement );

	renderer.gammaInput = true;
	renderer.gammaOutput = true;

	// EVENTS
	window.addEventListener( 'resize', onWindowResize, false );

	// CONTROLS
	cameraControls = new THREE.OrbitAndPanControls( camera, renderer.domElement );
	cameraControls.target.set(0, 0, 0);

	// GUI
	setupGui();
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// EVENT HANDLERS

function onWindowResize() 
{
	var canvasWidth = window.innerWidth;
	var canvasHeight = window.innerHeight;
	renderer.setSize( canvasWidth, canvasHeight );
	camera.aspect = canvasWidth/ canvasHeight;
	camera.updateProjectionMatrix();
}

function setupGui() 
{
	effectController = 
	{
		rotatez: 45,
		rotatex: 45,
	};

	var h;
	var gui = new dat.GUI();

	// tree
	h = gui.addFolder( "Tree branch rotations" );
	h.add( effectController, "rotatez", 0, 360, 1 ).name("branch rotate z");
	h.add( effectController, "rotatex", 0, 360, 1 ).name("branch rotate x");
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// BASIC SCENE-STARTUP FUNCTIONS

function animate() 
{
	requestAnimationFrame( animate );
	render();
}

function render() 
{
	var delta = clock.getDelta();

	cameraControls.update( delta );
	
	// Change tree if applicable
	if ( effectController.rotatez != RotateZ || effectController.rotatex != RotateX)
	{
		RotateZ = effectController.rotatez;
		RotateX = effectController.rotatex;
		fillScene();
	}
	
	// Age lights
	age();
	
	renderer.render( scene, camera );
}

function fillScene() 
{
	// SCENE 
	scene = new THREE.Scene();
	
	// CAMERA
	scene.add(camera);
	
	// LIGHTS
	setupLights();
	
	// SKY
	addSkyBox();
	
	// OBJECTS
	makePlane();
	makeForest();
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// METHODS THAT MAKE IT HAPPEN

init();
animate();

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
