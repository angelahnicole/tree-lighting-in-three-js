# README #

Using the Teapot Demo from Udacity, we'll create a forest. The trees can be tweaked by using the UI (just their base-rotations, as these are random). It will also add some best lighting method that I could find using Three JS.

Included in the source is also a document that I created that details the progression of the project.

![tree-lighting.PNG](https://bitbucket.org/repo/ynyxRM/images/4102989113-tree-lighting.PNG)

### How do I get set up? ###

You need to have a WebGL enabled browser. Additionally, you need to make sure that your browser can read local files. From [this site](http://www.chrome-allow-file-access-from-file.com/):

**On Windows Operating System**

* Get the url of your Chrome Installation  path to your chrome installation e.g C:\Users\-your-user-name\AppData\Local\Google\Chrome\Application>
* Launch the Google Chrome browser from the command line window with the additional argument ‘–allow-file-access-from-files’. E.g ‘path to your chrome installation\chrome.exe --allow-file-access-from-files’
* Temporary method you can use each time you are testing
* Copy the existing chrome launcher
* Do as above and save it with a new name e.g chrome - testing
* Alternatively, you can simply create a new launcher with the above and use it to start chrome.

**On Linux Operating System (specifically UBUNTU)**

* Go to the menu entry/ launcher for Chrome (.desktop file)
* Open the launcher properties dialog.
* It should look something like this: ‘/usr/bin/google-chrome %U’
* Change it to ‘/usr/bin/google-chrome --allow-access-from-files‘ to make the flags work permanently
* You may also need to delete and re-pin your launcher(s) after modifying it. Chrome should launch with the specified flags enabled after the modification.

Once all of that is done, you can see this project in action by simply clicking "index.html".